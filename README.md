# todo-list

# Get all available todos
app.get("/todos")

# Post new todo
app.post("/todos/") 

# Update todo checked state
app.put("/todos/:id")

# Delete todo
app.delete("/todos/:id")