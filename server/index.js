const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require("body-parser");

const todos = [];
let runningId = 0;
app.use(cors())

app.use(bodyParser.json());

app.get("/todos", (req, res) => {
  res.json(todos);
});

app.post("/todos/", (req, res) => {
  runningId++;
  const body = {
    id: runningId,
    text: req.body.text,
    completed: req.body.completed
  };
  todos.push(body);
  res.status(201).json(body);
  console.log(todos)
});

app.put("/todos/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const todo = todos.find((t) => t.id === id);
  if (!todo) {
    return res.status(404).json({ error: "Todo not found" });
  }
  todo.completed = !todo.completed;
  res.json(todo);
});

app.delete("/todos/:id", (req, res) => {
  const id = parseInt(req.params.id);
  const index = todos.findIndex((t) => t.id === id);
  if (index === -1) {
    return res.status(404).json({ error: "Todo not found" });
  }
  todos.splice(index, 1);
  res.status(204).send();
});

app.listen(8080, () => {
      console.log('server listening on port 8080')
})