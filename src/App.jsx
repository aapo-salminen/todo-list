import axios from 'axios';
import './App.css';
import Notes from './Notes.jsx';

function App() {

  return (
    <div className="App">
      <header className="App-header">
        <h2 id="header">Todos</h2>
        <div>
          <Notes/>
        </div>
      </header>
    </div>
  );
}

export default App