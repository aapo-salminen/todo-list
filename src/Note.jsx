import React, { useState, useEffect } from 'react'

function Note(props) {
    const { note, toggleCompletion } = props;
    return (
        <div
            className={note.completed ? "note checked-note" : "note"}
        >{note.text}
            <input
                type="checkbox"
                className="checkbox"
                checked={note.completed}
                onChange={() => toggleCompletion(note.id)}
            />        
        </div>       
    )
}

export default Note