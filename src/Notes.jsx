import axios from 'axios';
import React, { useState, useEffect } from 'react'
import './App.css';
import Note from './Note.jsx';

const todoUrl = "http://localhost:8080/";
let todosFromServer = [];

function Notes() {
  useEffect(() => {
    getNotes();
  }, [])

  async function getNotes() {
    axios.get(todoUrl + 'todos').then((response) => {
      todosFromServer = response.data;
      setTodos(todosFromServer);
    })
  }

  const [todos, setTodos] = useState(todosFromServer);
  const [note, setNote] = useState();

  const handleSubmit = (event) => {
    axios.post(todoUrl + 'todos', {
      text: note,
      completed: false
    })
      .then(function (response) {
        console.log("getnotes")
      })
      .catch(function (error) {
        console.log(error);
      });
    getNotes();
  }

  const toggleCompletion = (id) => {
    const newTodos = todos.map(todo => {
      if (todo.id !== id) {
        return todo
      } else
        updateToggleStateInServer(todo.id)
      return { id: todo.id, text: todo.text, completed: !todo.completed }
    });
    setTodos(newTodos);
  }

  const updateToggleStateInServer = async (id) => {
    try {
      await axios.put(todoUrl + "todos/" + id);
      console.log("Todo updated:", id);
    } catch (error) {
      console.error("Error updating todo:", error);
    }
  };

  const deleteTodos = () => {
    const newTodos = [...todos]
    for (let i = newTodos.length - 1; i >= 0; i--) {
      if (newTodos[i].completed) {
        deleteTodoFromServer(newTodos[i].id);
        newTodos.splice(i, 1);
      }
    }
    return setTodos(newTodos);
  }

  const deleteTodoFromServer = async (id) => {
    try {
      await axios.delete(todoUrl + "todos/" + id);
      console.log("Todo deleted:", id);
    } catch (error) {
      console.error("Error deleting todo:", error);
    }
  };

  return (
    <div className="App">
      <form onSubmit={handleSubmit}>
        <label>
          <textarea
            type="text"
            value={note}
            onChange={(e) => setNote(e.target.value)}
          />
        </label>
        <div id='button-holder'>
          <input type="submit" id='submit-button' value="Add" />
          <button onClick={deleteTodos} type="button" id='delete-button'>X</button>
        </div>
      </form>
     
      <div className="note-frame">
        <div>
          {todos.map(note =>
            <Note
              note={note}
              key={note.id}
              toggleCompletion={toggleCompletion}
            />
          )}
        </div>
      </div>
    </div>
  );
}

export default Notes